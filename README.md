# nanoAOD-open3d

An experiment visualizing nanoAOD with [Open3D](http://www.open3d.org/).

# Install Open3D

It's recommended to install a virtual environment:

Using `conda`:

```
conda install -n open3d
conda activate open3d
```

`pip install open3d`

### Problems with PyYAML

`pip install open3d --ignore-installed PyYAML`

You may want to specify a version of `pandas`:

`pip install open3d pandas>=2.7.3 --ignore-installed PyYAML` 
